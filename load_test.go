package config

import (
	"os"
	"testing"
)

func TestLoad(t *testing.T) {
	os.Setenv("dbhost", "localhost")
	cfgs, err := LoadIfExist("./test_cfg.ini")
	if err != nil {
		t.Error(err)
	}

	if cfgs.Has("no_exist_key") {
		t.Errorf("no exist key error")
	}
	if cfgs.Get("dbhost") != "localhost" {
		t.Errorf("dbhost should be override by envs")
	}
}
