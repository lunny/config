package config

import "errors"

var (
	// ErrConfigFileNotExist reprents the error
	ErrConfigFileNotExist = errors.New("config file is not exist")
)
