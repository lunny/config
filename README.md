# config

[![Build Status](https://drone.gitea.com/api/badges/lunny/config/status.svg)](https://drone.gitea.com/lunny/config)
[![](https://goreportcard.com/badge/gitea.com/lunny/config)](https://goreportcard.com/report/gitea.com/lunny/config)
[![GoDoc](https://godoc.org/gitea.com/lunny/config?status.png)](https://godoc.org/gitea.com/lunny/config)

Config is a simple config package to load config items from files, command line flags and enviroment variables.

[简体中文](https://gitea.com/lunny/config/blob/master/README_CN.md)

## Installation

```
go get gitea.com/lunny/config
```

## Example

The config format is simliar with ini but simpler(don't support sections), like below:

```ini
a=b
c=d
```

```Go
// load config items from file, envs or command line flags
cfgs, err := Load("config.ini")
if err != nil {
    t.Error(err)
}

// if you want to ignore the error if file is not exist, then it will read envs or command line flags
cfgs, err := LoadIfExist("config.ini")
if err != nil {
    t.Error(err)
}
```

Load config from flags:

```Go
cfgs := New(LoadFlags())
```

Load config from envs:

```Go
cfgs := New(LoadEnvs())
```