package config

import (
	"io/ioutil"
	"os"
	"strings"
)

// New creates a config object from maps
func New(data ...map[string]string) *Config {
	var cfg = Config(mergeMaps(data...))
	return &cfg
}

// Load loads config items from files, envs, flags
func Load(fPath string) (*Config, error) {
	cfg, err := loadFile(fPath)
	if err != nil {
		return nil, err
	}
	cfgEnvs := LoadEnvs()
	cfgFlags := LoadFlags()
	return New(cfg, cfgEnvs, cfgFlags), nil
}

// LoadFlags loads config items from commandline flags
func LoadFlags() map[string]string {
	var cfg = make(map[string]string)
	for _, f := range os.Args[1:] {
		f = strings.TrimLeft(f, "-")
		slice := splitEqualStr(f)
		if len(slice) >= 2 {
			cfg[slice[0]] = slice[1]
		} else if len(slice) > 0 {
			cfg[slice[0]] = ""
		}
	}
	return cfg
}

// LoadEnvs loads config items from envs
func LoadEnvs() map[string]string {
	var cfg = make(map[string]string)
	for _, e := range os.Environ() {
		slice := splitEqualStr(e)
		if len(slice) >= 2 {
			cfg[slice[0]] = slice[1]
		} else if len(slice) > 0 {
			cfg[slice[0]] = ""
		}
	}
	return cfg
}

func loadFile(fPath string) (map[string]string, error) {
	data, err := ioutil.ReadFile(fPath)
	if err != nil {
		return nil, err
	}
	if len(data) > 3 && data[0] == 0xEF && data[1] == 0xBB && data[2] == 0xBF {
		return parse(string(data[3:])), nil
	}
	return parse(string(data)), nil
}

// LoadFiles loads config items from files
func LoadFiles(fPaths ...string) (*Config, error) {
	if len(fPaths) == 0 {
		return nil, ErrConfigFileNotExist
	}
	var cfgs = make([]map[string]string, 0, len(fPaths))
	for _, f := range fPaths {
		cfg, err := loadFile(f)
		if err != nil {
			return nil, err
		}
		cfgs = append(cfgs, cfg)
	}
	return New(cfgs...), nil
}

func loadFileIfExist(fPath string) (map[string]string, error) {
	data, err := ioutil.ReadFile(fPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}
	if len(data) > 3 && data[0] == 0xEF && data[1] == 0xBB && data[2] == 0xBF {
		return parse(string(data[3:])), nil
	}
	return parse(string(data)), nil
}

// LoadIfExist will load files if exist and then environments and then flags
func LoadIfExist(fPaths ...string) (*Config, error) {
	var cfgs = make([]map[string]string, 0, len(fPaths))
	for _, f := range fPaths {
		cfg, err := loadFileIfExist(f)
		if err != nil {
			return nil, err
		}
		if cfg != nil {
			cfgs = append(cfgs, cfg)
		}
	}
	cfgs = append(cfgs, LoadEnvs(), LoadFlags())
	return New(cfgs...), nil
}

func mergeMaps(maps ...map[string]string) map[string]string {
	if len(maps) == 0 {
		return map[string]string{}
	}
	for _, m := range maps[1:] {
		for k, v := range m {
			maps[0][k] = v
		}
	}
	return maps[0]
}

func splitEqualStr(line string) []string {
	line = strings.TrimRight(line, "\r")
	vs := strings.SplitN(line, "=", 2)
	for i := 0; i < len(vs); i++ {
		vs[i] = strings.TrimSpace(vs[i])
	}
	return vs
}

func parse(data string) map[string]string {
	configs := make(map[string]string)
	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, ";") || strings.HasPrefix(line, "#") {
			continue
		}

		vs := splitEqualStr(line)
		if len(vs) >= 2 {
			configs[vs[0]] = vs[1]
		}
	}
	return configs
}

// Parse parses data to config
func Parse(data string) *Config {
	return New(parse(data))
}
