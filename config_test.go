package config

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLoadFiles(t *testing.T) {
	cfgs, err := LoadFiles("./test_cfg.ini")
	if err != nil {
		t.Error(err)
	}

	if cfgs.Has("no_exist_key") {
		t.Errorf("no exist key error")
	}

	if cfgs.MustString("no_exist_key") != "" {
		t.Error("no exist key error")
	}

	if cfgs.MustString("no_exist_key", "1") != "1" {
		t.Error("no exist key error")
	}

	if v := cfgs.Get("dbhost"); len(v) > 0 {
		t.Errorf("key dbhost should be empty")
	}

	if cfgs.MustString("dbhost") != "" {
		t.Error("key dbhost should be empty")
	}

	if cfgs.MustString("dbhost", "1") != "" {
		t.Error("key dbhost should be empty")
	}

	if v, _ := cfgs.GetBool("usecache"); v {
		t.Errorf("key usecache should be false")
	}

	if cfgs.MustBool("usecache") {
		t.Errorf("key usecache should be false")
	}

	if v, _ := cfgs.GetInt("mgrPort"); v != 8866 {
		t.Errorf("key mgrPort should be int and equal 8866")
	}

	if cfgs.MustInt("mgrPort") != 8866 {
		t.Errorf("key mgrPort should be int and equal 8866")
	}

	if v, _ := cfgs.GetTimeDuration("timeout"); v != time.Second*10 {
		t.Errorf("key timeout should be time.Duration and equal 10 * time.Second")
	}

	if cfgs.MustTimeDuration("timeout") != time.Second*10 {
		t.Errorf("key timeout should be time.Duration and equal 10 * time.Second")
	}

	assert.EqualValues(t, 1, cfgs.MustInt("blank"))

	m := cfgs.Map()

	assert.EqualValues(t, 14, len(m), "config items on file")
}
