# config

[![Build Status](https://drone.gitea.com/api/badges/lunny/config/status.svg)](https://drone.gitea.com/lunny/config)
[![](https://goreportcard.com/badge/gitea.com/lunny/config)](https://goreportcard.com/report/gitea.com/lunny/config)
[![GoDoc](https://godoc.org/gitea.com/lunny/config?status.png)](https://godoc.org/gitea.com/lunny/config)

config 是一个简单的配置管理包，可以从多个文件，环境变量和命令行参数读取配置项。

[English](https://gitea.com/lunny/config/blob/master/README.md)

## 安装

```
go get gitea.com/lunny/config
```

## 例子

配置文件的格式如下：

```ini
a=b
c=d
```

```Go
// load config items from file, envs or command line flags
cfgs, err := Load("config.ini")
if err != nil {
    t.Error(err)
}
```

从命令行参数加载:

```Go
cfgs := New(LoadFlags())
```

从环境变量加载:

```Go
cfgs := New(LoadEnvs())
```