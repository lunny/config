/*

Config is a simple k/v config package to load config items from files, command line flags and enviroment variables.

Installation

    go get gitea.com/lunny/config

Load config items

	// This will load configs from files, envs, flags and will be overrided according files < envs < flags
	cfgs, err := config.Load(myconfigfile1, myconfigfile2)

	// Or if you want to ignore the error when files do not exist.
	cfgs, err := config.LoadIfExist(myconfigfile)

Usage

	cfgs.Get(key)
	cfgs.Set(key, value)
	cfgs.MustString(key)
	cfgs.MustInt(key)
*/
package config
