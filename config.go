package config

import (
	"strconv"
	"strings"
	"time"
)

// Config config object
type Config map[string]string

// Set sets a config item
func (config *Config) Set(key, value string) {
	(*config)[key] = value
}

// Get returns a config item
func (config *Config) Get(key string) string {
	return (*config)[key]
}

// MustString return the string value
func (config *Config) MustString(key string, defaultVal ...string) string {
	if v, ok := (*config)[key]; ok {
		return v
	} else if len(defaultVal) > 0 {
		return defaultVal[0]
	}
	return ""
}

// Has returned if the key existed
func (config *Config) Has(key string) bool {
	_, ok := (*config)[key]
	return ok
}

// GetInt returns item value as int
func (config *Config) GetInt(key string) (int, error) {
	v := config.Get(key)
	return strconv.Atoi(v)
}

// MustInt always returns item value as int
func (config *Config) MustInt(key string, defaultVal ...int) int {
	if v, ok := (*config)[key]; ok {
		r, err := strconv.Atoi(v)
		if err == nil {
			return r
		}
	}

	if len(defaultVal) > 0 {
		return defaultVal[0]
	}
	return 0
}

// MustInt64 always returns item value as int64
func (config *Config) MustInt64(key string, defaultVal ...int64) int64 {
	if v, ok := (*config)[key]; ok {
		r, err := strconv.ParseInt(v, 10, 64)
		if err == nil {
			return r
		}
	}
	if len(defaultVal) > 0 {
		return defaultVal[0]
	}
	return 0
}

// GetBool returns item value as bool
func (config *Config) GetBool(key string) (bool, error) {
	v := config.Get(key)
	return strconv.ParseBool(v)
}

// MustBool always returns item value as bool
func (config *Config) MustBool(key string, defaultVal ...bool) bool {
	if v, ok := (*config)[key]; ok {
		r, err := strconv.ParseBool(v)
		if err == nil {
			return r
		}
	}
	if len(defaultVal) > 0 {
		return defaultVal[0]
	}
	return false
}

// GetTimeDuration returns item value as time.Duration
func (config *Config) GetTimeDuration(key string) (time.Duration, error) {
	v := config.Get(key)
	return time.ParseDuration(v)
}

// MustTimeDuration always returns item value as time.Duration
func (config *Config) MustTimeDuration(key string, defaultVal ...time.Duration) time.Duration {
	if v, ok := (*config)[key]; ok {
		d, err := time.ParseDuration(v)
		if err == nil {
			return d
		}
	}
	if len(defaultVal) > 0 {
		return defaultVal[0]
	}
	return 0
}

// GetFloat64 returns item value as float64
func (config *Config) GetFloat64(key string) (float64, error) {
	return strconv.ParseFloat(config.Get(key), 64)
}

// MustFloat64 always returns item value as float64
func (config *Config) MustFloat64(key string, defaultVal ...float64) float64 {
	if v, ok := (*config)[key]; ok {
		r, err := strconv.ParseFloat(v, 64)
		if err == nil {
			return r
		}
	}
	if len(defaultVal) > 0 {
		return defaultVal[0]
	}
	return 0
}

// GetSlice returns item value as a slice
func (config *Config) GetSlice(key, sep string) []string {
	return strings.Split(config.Get(key), sep)
}

// Map returns all the item key and value as a map
func (config *Config) Map() map[string]string {
	return *config
}
